/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_server.sockets;

import com.google.gson.Gson;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import virus_server.controller.ControlPartida;
import virus_server.model.JugadorLinea;
import virus_server.model.MensajeDto;

public class Server {

    //initialize socket and input stream 
    private Socket socket = null;
    private ServerSocket server = null;
    private DataInputStream in = null;
    private DataInputStream input = null;
    private DataOutputStream out = null;
    private Gson json;
    //datos del controllador de partida
    private ControlPartida control;
    private Boolean enPartida = false;
    //datos de envio o mensaje de peticion o reaccion
    private MensajeDto mensaje;
    //numero de jugadores registrados
    private Integer numeroRegistro = 0;
    //controll de mensajes
    private List<MensajeDto> mensajesHistorial = new ArrayList<MensajeDto>();
    private List<Thread> hilos = new ArrayList<Thread>();

    private Integer tiempo = 3;
    private Boolean enEsperaPartida = false;

    public Server() throws IOException {
        control = new ControlPartida();
        json = new Gson();
        //recibirReaccion(10100);
        recibirPeticiones(10000);

    }

    public static void main() {

    }

    /**
     * Crea el hilo para recibir reacciones de los direrentes juadores
     *
     * @param port
     */
    private void recibirReaccion(Integer port) {
        try {
            server = new ServerSocket(port);
            System.out.println("Server started reaccione ->" + port.toString());
            Thread t = new Thread(() -> {
                while (true) {
                    try {
                        System.out.println("Waiting for a client ...");
                        socket = server.accept();
                        System.out.println("Client accepted");

                        // takes input from the client socket
                        in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

                        String jsonDato = "";
                        try {
                            jsonDato = in.readUTF();
                            mensaje = json.fromJson(jsonDato, MensajeDto.class);

                        } catch (IOException i) {
                            System.out.println(i);
                        }

                        // close connection
                        socket.close();
                        in.close();
                    } catch (IOException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            hilos.add(t);
            t.start();
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    /**
     * Recibe las peticiones de los jugadores como unirse a partida
     *
     * @param port
     */
    private void recibirPeticiones(Integer port) {
        // starts server and waits for a connection 
        try {
            server = new ServerSocket(port);
            System.out.println("Server started peticiones ->" + port.toString());
            Thread t = new Thread(() -> {
                while (true) {
                    try {
                        System.out.println("Waiting for a client ...");
                        socket = server.accept();
                        System.out.println("Client accepted");

                        // takes input from the client socket
                        in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

                        String jsonDato = "";
                        try {
                            jsonDato = in.readUTF();
                            mensaje = json.fromJson(jsonDato, MensajeDto.class);
                            evaluarPeticion(mensaje);

                        } catch (IOException i) {
                            System.out.println(i);
                        }

                        // close connection
                        socket.close();
                        in.close();
                    } catch (IOException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            hilos.add(t);
            t.start();
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    private void evaluarPeticion(MensajeDto msj) {
        Boolean error = false;
        enEsperaPartida = false;

        if (msj.getPeticion().equalsIgnoreCase("UnirmePartida") && !enPartida && control.getJugadoresLinea().size() < 6) {
            JugadorLinea j1 = seDebeEnviarDatos(msj);
            if (buscarEnLista(j1)) {
                msj.setPeticion("PasarPantallaEspera");
                msj.setMsjServidorCliente("AgregadoListaEspera");
                control.getJugadoresLinea().add(j1);
                j1.setEnviarDatos(true);
                numeroRegistro++;

            } else {
                msj.setPeticion("MostarErrorAgregarNombreExistente");
                msj.setMsjServidorCliente("ErrorAgregarNombreExistente");
                error = true;
            }
            enviarRespuetaUno(msj);
            verificarCantidadJugadores(error);
        } else {
            mensajeFueraTiempoRegistro(msj);
        }
        if (enPartida && msj.getPeticion().equalsIgnoreCase("CambioTurnoYactualizarDatos")) {
            control.setJugadoresLinea(msj.getJugadoresEnLinea());
            control.setCartasDesecho(msj.getCartasDesecho());
            control.cambiarTurno();
            actualizarYcambiarTurno();
        }

    }
    private void actualizarYcambiarTurno() {
        //Parte de crear mensaje para todos
        MensajeDto msj = new MensajeDto();
        msj.setPeticion("AcutalizarInterDatosJuegoYcambioTurno");
        msj.setJugadoresEnLinea(control.getJugadoresLinea());
        msj.setCartasDesecho(control.getCartasDesecho());
        //Parte de cargar cartas 
        control.cargarCartasCadaJugador();
        //Parte de enviar a todos
        Thread t = new Thread(() -> {
            enviarActualizacionTodos(msj, msj.getPeticion());
        });
        t.start();
    }

    private void verificarCantidadJugadores(Boolean error) {
        if (!error && !enEsperaPartida) {

            try {
                Thread t = new Thread(() -> {
                    if (control.getJugadoresLinea().size() >= 1) {
                        enEsperaPartida = true;
                        MensajeDto msj1 = new MensajeDto();
                        msj1.setTiempoInicioPartida(tiempo.toString());
                        msj1.setPeticion("MostrarTiempoInicioPartida");
                        msj1.setMensaje("El juego empezara en unos momentos... Esperando mas jugadores\n "
                                + "Cantidad de jugadores acutal: " + control.getJugadoresLinea().size());
                        msj1.getJugadoresEnLinea().clear();
                        msj1.setJugadoresEnLinea(control.getJugadoresLinea());
                        enviarActualizacionTodos(msj1, msj1.getPeticion());
                        iniciarTiempoInicioPartida();
                    }
                });
                t.sleep(1000);
                t.start();
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void iniciarTiempoInicioPartida(){
        Runnable runnable = () -> {
            while (tiempo > 0) {
                try {
                    Thread.sleep(1000);
                    System.out.println("El tiempo transcurrido es-> " + tiempo.toString());
                    tiempo--;
                    if (tiempo <= 0) {
                        enPartida = true;
                        iniciarPartida();
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        Thread t = new Thread(runnable);
        t.start();

    }
    private void iniciarPartida(){
        //Parte de crear mensaje para todos
        MensajeDto msj = new MensajeDto();
        msj.setPeticion("InciarPartidaCambioModoJuego");
        msj.setJugadoresEnLinea(control.getJugadoresLinea());
        msj.setCartasDesecho(control.getCartasDesecho());
        //Parte de cargar cartas 
        control.cargarCartasCadaJugador();
        control.cargarDatosPruebas();
        //Parte de enviar a todos
        Thread t =  new Thread(() -> {
         enviarActualizacionTodos(msj, msj.getPeticion());
        });
        t.start();
        
    }
    private void mensajeFueraTiempoRegistro(MensajeDto msj) {
        JugadorLinea j1 = seDebeEnviarDatos(msj);
        msj.setMsjServidorCliente("Partidallena");
        msj.setPeticion("MostrarMensajeServidorClienteFueraTimepo");
        msj.setMensaje("La partida ya esta llena, debe esperar a que termine para intetar de nuevo");
        j1.setEnviarDatos(true);
        enviarRespuetaUno(msj);
    }

    /**
     * Envia una acutalizacion general de datos a todos los jugadores
     */
    public void enviarActualizacionTodos(MensajeDto msj, String peticion) {
        for (int i = 0; i < control.getJugadoresLinea().size(); i++) {
            JugadorLinea j = msj.getJugadoresEnLinea().get(i);
            msj.setPeticion(peticion);
            enviarDatosTodos(msj, j);
        }
    }

    public void enviarDatosTodos(MensajeDto msj, JugadorLinea j) {
        // establish a connection 
        j.setEnviarDatos(false);

        try {

            socket = new Socket(j.getIpCliente(), j.getPuertoCliente());
            System.out.println("Connected");

            // takes input from terminal 
            input = new DataInputStream(System.in);

            // sends output to the socket 
            out = new DataOutputStream(socket.getOutputStream());
        } catch (UnknownHostException u) {
            System.out.println(u);
        } catch (IOException i) {
            System.out.println(i);
        }

        // string to read message from input 
        String jsonMensaje = "";

        try {
            jsonMensaje = json.toJson(msj);
            out.writeUTF(jsonMensaje);
        } catch (IOException i) {
            System.out.println(i);
        }

        // close the connection 
        try {
            input.close();
            out.close();
            socket.close();

        } catch (IOException i) {
            System.out.println(i);
        }

    }

    /**
     * Envia una respuesta a un jugador especifico
     */
    public void enviarRespuetaUno(MensajeDto msj) {
        enviarDatos(msj);
    }

    private JugadorLinea seDebeEnviarDatos(MensajeDto msj) {

        for (JugadorLinea j1 : msj.getJugadoresEnLinea()) {
            if (j1.getEnviarDatos()) {
                return j1;
            }
        }
        return null;
    }

    public void enviarDatos(MensajeDto msj) {
        // establish a connection 
        Thread t = new Thread(() -> {
            JugadorLinea j = seDebeEnviarDatos(msj);
            if (j != null) {
                j.setEnviarDatos(false);
                try {
                    socket = new Socket(j.getIpCliente(), j.getPuertoCliente());
                    System.out.println("Connected");

                    // takes input from terminal 
                    input = new DataInputStream(System.in);

                    // sends output to the socket 
                    out = new DataOutputStream(socket.getOutputStream());
                } catch (UnknownHostException u) {
                    System.out.println(u);
                } catch (IOException i) {
                    System.out.println(i);
                }

                // string to read message from input 
                String jsonMensaje = "";

                try {
                    jsonMensaje = json.toJson(msj);
                    out.writeUTF(jsonMensaje);
                } catch (IOException i) {
                    System.out.println(i);
                }

                // close the connection 
                try {
                    input.close();
                    out.close();
                    socket.close();
                } catch (IOException i) {
                    System.out.println(i);
                }
            }
        });
        t.start();
    }

    private Boolean buscarEnLista(JugadorLinea j1) {
        for (JugadorLinea j : control.getJugadoresLinea()) {
            if (j.getNombre().equalsIgnoreCase(j1.getNombre()) || j.getCodigo().equalsIgnoreCase(j1.getCodigo())) {
                return false;
            }
        }
        return true;
    }
}
