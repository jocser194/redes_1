/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_server.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jocse
 */
public class JugadorLinea {

    //datos de el servidor local 
    @SerializedName("ipCliente")
    private String ipCliente;
    @SerializedName("puertoCliente")
    private int puertoCliente;
    //enviar datos a este jugador
    @SerializedName("enviarDatos")
    private Boolean enviarDatos = false;
    //datos de el jugador
    @SerializedName("nombreJudador")
    private String nombre;
    @SerializedName("codigo")
    private String codigo;
    @SerializedName("MensajeServidorCliente")
    private String mensajeServidorCliente;
    //control de cartas jugador
    @SerializedName("cartasMano")
    private List<Integer> cartasMano = new ArrayList<Integer>();
    @SerializedName("cartasVisibleMesa")
    private List<Integer> cartasVisibleMesa = new ArrayList<Integer>();
    @SerializedName("cartasTotalMesa")
    private List<Integer> cartasTotalMesa = new ArrayList<Integer>();
    @SerializedName("cartasDesecho")
    private List<Integer> cartasDesecho = new ArrayList<Integer>(); 
    
    //lista Cartas divididas por tipo organo
    @SerializedName("cartasMesaOrgano1")
    private List<Integer> cartasMesaOrgano1 = new ArrayList<Integer>();
    @SerializedName("cartasMesaOrgano2")
    private List<Integer> cartasMesaOrgano2 = new ArrayList<Integer>();
    @SerializedName("cartasMesaOrgano3")
    private List<Integer> cartasMesaOrgano3 = new ArrayList<Integer>();
    @SerializedName("cartasMesaOrgano4")
    private List<Integer> cartasMesaOrgano4 = new ArrayList<Integer>(); 
    @SerializedName("turno")
    private Integer turno;

    public JugadorLinea() {
      this.turno =  0;    
    }

    public String getIpCliente() {
        return ipCliente;
    }

    public void setIpCliente(String ipCliente) {
        this.ipCliente = ipCliente;
    }

    public int getPuertoCliente() {
        return puertoCliente;
    }

    public void setPuertoCliente(int puertoCliente) {
        this.puertoCliente = puertoCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensajeServidorCliente() {
        return mensajeServidorCliente;
    }

    public void setMensajeServidorCliente(String mensajeServidorCliente) {
        this.mensajeServidorCliente = mensajeServidorCliente;
    }

    public List<Integer> getCartasMano() {
        return cartasMano;
    }

    public void setCartasMano(List<Integer> cartasMano) {
        this.cartasMano = cartasMano;
    }

    public List<Integer> getCartasTotalMesa() {
        return cartasTotalMesa;
    }

    public void setCartasTotalMesa(List<Integer> cartasTotalMesa) {
        this.cartasTotalMesa = cartasTotalMesa;
    }

    public List<Integer> getCartasVisibleMesa() {
        return cartasVisibleMesa;
    }

    public void setCartasVisibleMesa(List<Integer> cartasVisibleMesa) {
        this.cartasVisibleMesa = cartasVisibleMesa;
    }

    public Boolean getEnviarDatos() {
        return enviarDatos;
    }

    public void setEnviarDatos(Boolean enviarDatos) {
        this.enviarDatos = enviarDatos;
    }

    public List<Integer> getCartasDesecho() {
        return cartasDesecho;
    }

    public void setCartasDesecho(List<Integer> cartasDesecho) {
        this.cartasDesecho = cartasDesecho;
    }

    public List<Integer> getCartasMesaOrgano1() {
        return cartasMesaOrgano1;
    }

    public void setCartasMesaOrgano1(List<Integer> cartasMesaOrgano1) {
        this.cartasMesaOrgano1 = cartasMesaOrgano1;
    }

    public List<Integer> getCartasMesaOrgano2() {
        return cartasMesaOrgano2;
    }

    public void setCartasMesaOrgano2(List<Integer> cartasMesaOrgano2) {
        this.cartasMesaOrgano2 = cartasMesaOrgano2;
    }

    public List<Integer> getCartasMesaOrgano3() {
        return cartasMesaOrgano3;
    }

    public void setCartasMesaOrgano3(List<Integer> cartasMesaOrgano3) {
        this.cartasMesaOrgano3 = cartasMesaOrgano3;
    }

    public List<Integer> getCartasMesaOrgano4() {
        return cartasMesaOrgano4;
    }

    public void setCartasMesaOrgano4(List<Integer> cartasMesaOrgano4) {
        this.cartasMesaOrgano4 = cartasMesaOrgano4;
    }

    public Integer getTurno() {
        return turno;
    }

    public void setTurno(Integer turno) {
        this.turno = turno;
    }

}
