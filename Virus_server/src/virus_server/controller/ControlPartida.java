/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_server.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import virus_server.model.JugadorLinea;


/**
 *
 * @author jocse
 */
public class ControlPartida{
    
    private Integer ultimaCartaRepartida = 0;
    private List<Integer> cartasLogicas = new ArrayList<Integer>();
    private List<Integer> cartasDesecho = new ArrayList<Integer>();
    private List<JugadorLinea> jugadoresLinea = new ArrayList<JugadorLinea>();
    private Integer turnoAcutal = 0;
     
    public ControlPartida(){
        cargarCartas();
    }

    private void cargarCartas(){
        for (int i = 0; i < 68; i++) {
            cartasLogicas.add(i);
        }
        Collections.shuffle(cartasLogicas);
    }

    /**
     *Retorna una lista de cartasLogicas dependiendo de la catidad restante y la solicitada
     * @param cantidad
     * @return
     */
    private List<Integer> repartirCartas(Integer cantidad) {
        List<Integer> cartas = new ArrayList<Integer>();
        for (int i = 0; i < cantidad; i++) {
            if (ultimaCartaRepartida <= cartasLogicas.size()) {
                cartas.add(cartasLogicas.get(ultimaCartaRepartida));
                ultimaCartaRepartida++;
            } else {
                i = cantidad;
            }
        }
        return cartas;
    }
    public void cargarCartasCadaJugador(){
        for (JugadorLinea j : jugadoresLinea) {
            j.setCartasMano(repartirCartas(3));
            j.setCartasDesecho(cartasDesecho);
        }
    }
    public void cambiarTurno(){
        if (turnoAcutal<jugadoresLinea.size()) {
            jugadoresLinea.get(turnoAcutal).setTurno(1);
            turnoAcutal++;
        }else{
            turnoAcutal = 0;
            jugadoresLinea.get(turnoAcutal);
        }
        
    }
    public void cargarDatosPruebas(){
        for (JugadorLinea j : jugadoresLinea) {
            j.setCartasMesaOrgano1(repartirCartas(2));
            j.setCartasMesaOrgano2(repartirCartas(2));
            j.setCartasMesaOrgano3(repartirCartas(2));
            j.setCartasMesaOrgano4(repartirCartas(2));
        }
    }
    public List<JugadorLinea> getJugadoresLinea() {
        return jugadoresLinea;
    }

    public void setJugadoresLinea(List<JugadorLinea> jugadoresLinea) {
        this.jugadoresLinea = jugadoresLinea;
    }

    public Integer getUltimaCartaRepartida() {
        return ultimaCartaRepartida;
    }

    public void setUltimaCartaRepartida(Integer ultimaCartaRepartida) {
        this.ultimaCartaRepartida = ultimaCartaRepartida;
    }

    public List<Integer> getCartasLogicas() {
        return cartasLogicas;
    }

    public void setCartasLogicas(List<Integer> cartasLogicas) {
        this.cartasLogicas = cartasLogicas;
    }

    public List<Integer> getCartasDesecho() {
        return cartasDesecho;
    }

    public void setCartasDesecho(List<Integer> cartasDesecho) {
        this.cartasDesecho = cartasDesecho;
    }
    
}
