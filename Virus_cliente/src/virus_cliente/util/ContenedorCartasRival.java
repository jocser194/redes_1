/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.util;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.layout.HBox;

/**
 *
 * @author jocse
 */
public class ContenedorCartasRival {
    
    private List<HBox> hbJugadoresRivales = new ArrayList<>();

    public ContenedorCartasRival() {
        for (int i = 0; i < 4; i++) {
            HBox h = new HBox();
            h.setSpacing(5);
            hbJugadoresRivales.add(h);
        }
    }

    public List<HBox> getHbJugadoresRivales() {
        return hbJugadoresRivales;
    }

    public void setHbJugadoresRivales(List<HBox> hbJugadoresRivales) {
        this.hbJugadoresRivales = hbJugadoresRivales;
    }
    
    
}
