/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.util;

import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 *
 * @author jocse
 */
public class Transicion {

    private ScaleTransition st;
    private ScaleTransition st2;
    private int n;

    public Transicion() {
        this.n = 1;
    }

    public void animasionCam(ImageView iv1, Image im1) {
        //inicio de la animacion para cambio de imagen
        //asignacion del tiempo de la animacion
        //datos de que animacion se va a realizar
        //evento play para que pueda correr
        this.st = new ScaleTransition(Duration.millis(100), iv1);
        this.st.setFromY(1.0);
        this.st.setToY(0.0);
        this.st.play();
        //despues de la animaicion de salida se debe hacer otra para mostrar la nueva imagen
        //lo mismo que arriba paro en proceso inverso
        //con el evento setOnFinishe de finalizacion para poder realizar al finalizar el primer evento
        this.st.setDelay(Duration.seconds(2));
        this.st.setOnFinished((event) -> {
            //se carga la imagen que se desea insertar despues de la animacion
            iv1.setImage(im1);
            this.st2 = new ScaleTransition(Duration.millis(100), iv1);
            this.st2.setFromY(0.0);
            this.st2.setToY(1.0);
            this.st2.play();

        });
    }

    public void animasionCamDealy(ImageView iv1, Image im1) {
        //inicio de la animacion para cambio de imagen
        //asignacion del tiempo de la animacion
        //datos de que animacion se va a realizar
        //evento play para que pueda correr
        this.st = new ScaleTransition(Duration.millis(100), iv1);
        //pausa para ver la carta actual
        this.st.setDelay(Duration.seconds(1));
        this.st.setFromY(1.0);
        this.st.setToY(0.0);
        this.st.play();
        //despues de la animaicion de salida se debe hacer otra para mostrar la nueva imagen
        //lo mismo que arriba paro en proceso inverso
        //con el evento setOnFinishe de finalizacion para poder realizar al finalizar el primer evento
        this.st.setOnFinished((event) -> {
            //se carga la imagen que se desea insertar despues de la animacion
            iv1.setImage(im1);
            this.st2 = new ScaleTransition(Duration.millis(100), iv1);
            this.st2.setFromY(0.0);
            this.st2.setToY(1.0);
            this.st2.play();
        });
    }

    public void aniCamJugador(Label iv3, String nombre) {
        FadeTransition ft = new FadeTransition(Duration.millis(150), iv3);
        ft.setDelay(Duration.seconds(1));
        ft.setFromValue(1);
        ft.setToValue(0);
        ft.play();
        ft.setOnFinished((event) -> {
            iv3.setText(nombre);
            FadeTransition ft2 = new FadeTransition(Duration.millis(150), iv3);
            ft2.setFromValue(0);
            ft2.setToValue(1);
            ft2.play();
        });
    }

    /**
     * efeto de contador de tiempo de turno
     *
     * @param iv3
     * @param tin
     */
    public void aniCamTiempo(Label lb3, int tin) {
        FadeTransition ft = new FadeTransition(Duration.millis(150), lb3);
        ft.setFromValue(1);
        ft.setToValue(0);
        ft.play();
        ft.setOnFinished((event) -> {

            if (tin <= 0) {
                lb3.setText("El juego iniciara en breve");
            }
            if (tin > 60) {
                lb3.setText("Falta mas de "+(tin / 60) + " minutos aproximadamente...");
            }
            if (tin > 0 && tin <60) {
                lb3.setText("Menos de 1 minutos aproximadamente...");
            }

            FadeTransition ft2 = new FadeTransition(Duration.millis(150), lb3);
            ft2.setFromValue(0);
            ft2.setToValue(1);
            ft2.play();
        });
    }

    public Boolean cargarImagen(ImageView im) {

        if (n == 1) {
            this.st = new ScaleTransition(Duration.millis(200), im);
            this.st.setFromY(1.0);
            this.st.setToY(0.0);
            this.st.play();
            this.st.setOnFinished((event) -> {
                this.st2 = new ScaleTransition(Duration.millis(200), im);
                this.st2.setFromY(0.0);
                this.st2.setToY(1.0);
                this.st2.play();
            });
            //para cambiar de animacion depende del cambio de carta
            n = 2;
        } else {

            this.st = new ScaleTransition(Duration.millis(200), im);
            this.st.setFromX(1.0);
            this.st.setToX(0.0);
            this.st.play();
            this.st.setOnFinished((event) -> {
                this.st2 = new ScaleTransition(Duration.millis(200), im);
                this.st2.setFromX(0.0);
                this.st2.setToX(1.0);
                this.st2.play();
            });
            n = 1;
        }
        return true;
    }
}
