/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.controller;

import com.google.gson.Gson;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import virus_cliente.model.Baraja;
import virus_cliente.model.JugadorLinea;
import virus_cliente.model.MensajeDto;
import virus_cliente.sockets.Client;
import virus_cliente.util.AppContext;
import virus_cliente.util.FlowController;
import virus_cliente.util.Mensaje;

/**
 * FXML Controller class
 *
 * @author jocse
 */
public class InicioController extends Controller implements Initializable {

    @FXML
    private AnchorPane root2;
    @FXML
    private ImageView ivInicio;
    @FXML
    private JFXButton btnInicio;

    private Client c;

    private JugadorLinea jL;

    private MensajeDto msj;
    @FXML
    private JFXTextField txtNombreJugador;

    private Baraja b;

    //datos iniciales de el sistema 
    private String ipCliente = "192.168.1.4";

    private Integer numeroPuertoLocal = 20100;

    private String ipServidor = "192.168.1.4";

    private Integer puertoDatosServidor = 10000;

    private Integer puertoReaccionServidor = 10100;

    private Gson g = new Gson();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ipLocal();
        this.ivInicio.fitHeightProperty().bind(this.root2.heightProperty());
        this.ivInicio.fitWidthProperty().bind(this.root2.widthProperty());
        AppContext.getInstance().set("InicioController", this);

        //datos de inicio para unir a partida
        c = new Client(ipServidor, puertoDatosServidor, puertoReaccionServidor, ipCliente, numeroPuertoLocal);
        //mensaje inicial para el servidor
        msj = new MensajeDto();
        msj.setPeticion("UnirmePartida");
        msj.setMetodo("AgregarPartida");
        jL = new JugadorLinea();
        jL.setIpCliente(ipCliente);
        jL.setPuertoCliente(numeroPuertoLocal);
        jL.setCodigo(ipCliente + numeroPuertoLocal.toString());
        b = new Baraja();
        AppContext.getInstance().set("Baraja", b);
        for (int i = 0; i < 5; i++) {
            jL.getCartasTotalMesa().add(0);
            jL.getCartasMano().add(0);
            jL.getCartasVisibleMesa().add(0);
        }

        msj.getJugadoresEnLinea().add(jL);
        String m = g.toJson(msj);
        System.out.println(msj.toString());

        //cargar para tener en todo el programa
        AppContext.getInstance().set("Cliente", c);
        AppContext.getInstance().set("Jugador", jL);
    }

    @FXML
    private void inicio(ActionEvent event) throws IOException {
        if (!txtNombreJugador.getText().isEmpty()) {
            jL.setNombre(txtNombreJugador.getText());
            jL.setEnviarDatos(true);
            c.enviarDatos(msj);
        } else {
            Mensaje.show(Alert.AlertType.ERROR, "Nombre necesario", "Se debe introducir un nombre de usuario");
        }

    }

    public void pasarPantallaEspera() {
        Platform.runLater(() -> {
            FlowController.getInstance().goMainPrincipal();
            FlowController.getInstance().goView("Menu", "Out", null);
            FlowController.getInstance().goView("Espera", "Center", null);
            ((Stage) btnInicio.getScene().getWindow()).close();
        });

    }

    @Override
    public void initialize() {

    }

    private void ipLocal() {
        try {
            InetAddress address = InetAddress.getLocalHost();
            ipCliente = address.getHostAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(InicioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
