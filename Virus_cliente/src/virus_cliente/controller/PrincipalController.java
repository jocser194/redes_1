/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import virus_cliente.controller.Controller;
import virus_cliente.util.AppContext;
import virus_cliente.util.FlowController;

/**
 * FXML Controller class
 *
 * @author jocse
 */
public class PrincipalController extends Controller implements Initializable {

    @FXML
    private StackPane root;
    @FXML
    private ImageView ivPrincipal;
    @FXML
    private BorderPane bpInicio;
    @FXML
    private JFXHamburger btnHamMenu;
    @FXML
    private JFXDrawer drwMenu;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AppContext.getInstance().set("menu", drwMenu);
        this.bpInicio.addEventHandler(MouseEvent.MOUSE_CLICKED, click);
        this.ivPrincipal.fitHeightProperty().bind(this.root.heightProperty());
        this.ivPrincipal.fitWidthProperty().bind(this.root.widthProperty());
        
    }

    @Override
    public void initialize() {

    }
    @FXML
    private void menu(MouseEvent event) {
        if(this.drwMenu.isShown()){
            this.drwMenu.close();
            this.drwMenu.setPrefWidth(0);
        }else{
            this.drwMenu.setPrefWidth(250);
            this.drwMenu.open();
        }
    }
    public void ocultarMenu() {
        if (this.drwMenu.isShown()) {
            this.drwMenu.close();
            this.drwMenu.setPrefWidth(0);
        }
    }
    EventHandler<MouseEvent> click = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            ocultarMenu();
        }

    };
}
