/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import virus_cliente.controller.Controller;
import virus_cliente.util.AppContext;

/**
 * FXML Controller class
 *
 * @author jocse
 */
public class MenuController extends Controller implements Initializable {

    @FXML
    private AnchorPane roor3;
    @FXML
    private VBox vbMenu;
    @FXML
    private JFXButton btnEmpresa;
    
    private JFXDrawer drwMenu;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.drwMenu = (JFXDrawer) AppContext.getInstance().get("menu");
    }    

    @FXML
    private void empresa(ActionEvent event) {
    }


    @Override
    public void initialize() {
        
    }
    public void ocultarMenu() {
        if (this.drwMenu.isShown()) {
            this.drwMenu.close();
            this.drwMenu.setPrefWidth(0);
        }
    }
}
