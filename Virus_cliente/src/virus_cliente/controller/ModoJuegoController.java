/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import virus_cliente.model.Baraja;
import virus_cliente.model.JugadorLinea;
import virus_cliente.model.MensajeDto;
import virus_cliente.sockets.Client;
import virus_cliente.util.AppContext;
import virus_cliente.util.ContenedorCartasRival;
import virus_cliente.util.MensajePopUp;

/**
 * FXML Controller class
 *
 * @author jocse
 */
public class ModoJuegoController extends Controller implements Initializable {

    @FXML
    private VBox vbJugador;
    @FXML
    private VBox vbRivales;
    @FXML
    private HBox vbCartasMesa;
    //muestra la baraja de cartas y las de desecho
    @FXML
    private AnchorPane apBarajaAndDesecho;
    //cartas que tiene en mano el jugador
    @FXML
    private HBox hbCartasMano;

    //Colocar las cartas de mesa de cada jugador
    @FXML
    private VBox vbOrganoColor1;
    @FXML
    private VBox vbOrganoColor2;
    @FXML
    private VBox vbOrganoColor3;
    @FXML
    private VBox vbOrganoColor4;

    private MensajeDto msj;

    private Baraja b;

    private JugadorLinea jL;

    private List<VBox> listCartasMesaRival = new ArrayList<>();

    private List<JugadorLinea> jugadoresRivales = new ArrayList<>();

    private Client c;

    private MensajePopUp msjPopUp = new MensajePopUp();

    private ContenedorCartasRival contenedor;

    private List<ContenedorCartasRival> contenedorList = new ArrayList<>();

    private List<HBox> hbJugadoresRivales = new ArrayList<>();

    private List<VBox> hbJugadoreLocal = new ArrayList<>();

    private List<VBox> vbJugadoresRivales = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        msj = (MensajeDto) AppContext.getInstance().get("Mensaje");
        jL = (JugadorLinea) AppContext.getInstance().get("Jugador");
        c = (Client) AppContext.getInstance().get("Cliente");
        b = (Baraja) AppContext.getInstance().get("Baraja");
        indicarRequeridosLocal();
        buscarJugadorLocal();
        
    }

    private void generarContenedorR() {
        
        for (int i = 0; i < jugadoresRivales.size(); i++) {
            contenedor = new ContenedorCartasRival();
            contenedorList.add(contenedor);
            contenedor.getHbJugadoresRivales().forEach((t) -> {
                vbRivales.getChildren().add(t);
            });

        }
    }

    @Override
    public void initialize() {
    }

    /**
     * Busca el jugador local asi como los rivales
     */
    private void buscarJugadorLocal() {
        for (JugadorLinea jugador : msj.getJugadoresEnLinea()) {
            if (jugador.getCodigo().equalsIgnoreCase(jL.getCodigo())) {
                jL = jugador;
                cargarCartasJLocal(jugador);
            } else {
                jugadoresRivales.add(jugador);
            }
        }
        generarContenedorR();
        cargarCartasRivales();
    }

    private void cargarCartasJLocal(JugadorLinea j) {

        hbCartasMano.getChildren().clear();
        vbOrganoColor1.getChildren().clear();
        vbOrganoColor2.getChildren().clear();
        vbOrganoColor3.getChildren().clear();
        vbOrganoColor4.getChildren().clear();

        for (Integer integer : j.getCartasMano()) {
            hbCartasMano.getChildren().add(b.getCarta(integer));
        }
        for (Integer integer : j.getCartasMesaOrgano1()) {
            vbOrganoColor1.getChildren().add(b.getCarta(integer));
        }
        for (Integer integer : j.getCartasMesaOrgano2()) {
            vbOrganoColor2.getChildren().add(b.getCarta(integer));
        }
        for (Integer integer : j.getCartasMesaOrgano3()) {
            vbOrganoColor3.getChildren().add(b.getCarta(integer));
        }
        for (Integer integer : j.getCartasMesaOrgano4()) {
            vbOrganoColor4.getChildren().add(b.getCarta(integer));
        }

    }

    private void cargarCartasRivales() {
        limpiarContenedoresR();
        for (int i = 0; i < jugadoresRivales.size(); i++) {

            for (Integer integer : jugadoresRivales.get(i).getCartasMesaOrgano1()) {
                cargarImagenesRivales(i, 0, integer);
            }
            for (Integer integer : jugadoresRivales.get(i).getCartasMesaOrgano2()) {
                cargarImagenesRivales(i, 1, integer);
            }
            for (Integer integer : jugadoresRivales.get(i).getCartasMesaOrgano3()) {
                cargarImagenesRivales(i, 2, integer);
            }
            for (Integer integer : jugadoresRivales.get(i).getCartasMesaOrgano4()) {
                cargarImagenesRivales(i, 3, integer);
            }

        }

    }

    private void limpiarContenedoresR() {
        for (ContenedorCartasRival contenedorCartasRival : contenedorList) {
            contenedorCartasRival.getHbJugadoresRivales().forEach((t) -> {
                t.getChildren().clear();
            });
        }
    }

    private void cargarImagenesRivales(Integer i, Integer pos, Integer carta) {
        contenedorList.get(i).getHbJugadoresRivales().get(pos).getChildren().add(b.getCarta(carta));
    }

    public void indicarRequeridosLocal() {
        this.hbJugadoreLocal.clear();
        this.hbJugadoreLocal.addAll(Arrays.asList(vbOrganoColor1, vbOrganoColor2, vbOrganoColor3, vbOrganoColor4));
    }

    public void actualizarDatos() {
//        cargarCartasJLocal();
        cargarCartasRivales();
    }

    private void enviarAAcutalizarYCambioTurno() {
        jL.setTurno(0);
        msj.setPeticion("CambioTurnoYactualizarDatos");
        c.enviarDatos(msj);

    }
}
