/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.controller;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import virus_cliente.model.MensajeDto;
import virus_cliente.util.AppContext;
import virus_cliente.util.FlowController;
import virus_cliente.util.Transicion;

/**
 * FXML Controller class
 *
 * @author jocse
 */
public class EsperaController extends Controller implements Initializable {

    @FXML
    private AnchorPane vistaEspera;
    @FXML
    private JFXButton btnInicio;
    @FXML
    private ImageView imgEspera;

    private Integer tiempo = 120;
    @FXML
    private Label lbTiempo;
    
    private Transicion tran = new Transicion();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AppContext.getInstance().set("EsperaController", this);
        this.imgEspera.fitHeightProperty().bind(this.vistaEspera.heightProperty());
        this.imgEspera.fitWidthProperty().bind(this.vistaEspera.widthProperty());
    }

    @Override
    public void initialize() {

    }

    public void iniciarTiempoInicioPartida(){
       MensajeDto msj = (MensajeDto) AppContext.getInstance().get("Mensaje");
       tiempo = Integer.valueOf(msj.getTiempoInicioPartida());
        
            Runnable runnable = () -> {
                while (tiempo >=0 ) {
                    try {
                        Thread.sleep(1000);
                        Platform.runLater(() -> {       tran.aniCamTiempo(lbTiempo, tiempo);});
                        tiempo--;
                    } catch (InterruptedException ex) {
                        Logger.getLogger(EsperaController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };
            Thread t = new Thread(runnable);
            t.start();
        
    }

    public void irPrincipal() {
        Platform.runLater(() -> {
            FlowController.getInstance().goView("Menu", "Out", null);
            FlowController.getInstance().goView("ModoJuego", "Center", null);
//            ((Stage) btnInicio.getScene().getWindow()).close();
        });

    }
}
