/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.sockets;
// A Java program for a Client 

import com.google.gson.Gson;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import virus_cliente.controller.EsperaController;
import virus_cliente.controller.InicioController;
import virus_cliente.model.MensajeDto;
import virus_cliente.util.AppContext;
import virus_cliente.util.MensajePopUp;

public class Client {

    // Datos para el cliente y servidor
    private Socket socket = null;
    private ServerSocket server = null;
    private DataInputStream in = null;
    private DataInputStream input = null;
    private DataOutputStream out = null;
    private String ipServidor;
    private int puertoDatosServiror;
    private int puertoReaccionServiror;
    private Gson json;
    private String ipCliente;
    private int puertoCliente;
    private List<Thread> hilos = new ArrayList<Thread>();

    //Datos para el cliente
    private MensajeDto mensaje;
    private MensajeDto reaccion;
    private MensajePopUp msjPopUp = new MensajePopUp();

    /**
     *
     * @param ipServidor ip del servidor
     * @param puertoDatosServiror puerto del servidor de datos
     * @param puertoReaccionServiror puerdo de reacciones de datos
     * @param ipcliente ip local
     * @param puertoCliente puerto local
     */
    public Client(String ipServidor, int puertoDatosServiror, int puertoReaccionServiror, String ipcliente, int puertoCliente) {

        this.ipServidor = ipServidor;
        this.puertoDatosServiror = puertoDatosServiror;
        this.puertoReaccionServiror = puertoReaccionServiror;
        this.ipCliente = ipcliente;
        this.puertoCliente = puertoCliente;
        this.json = new Gson();
        this.recibirActualizacionDatos(puertoCliente);
        AppContext.getInstance().set("PuertosEscucha", hilos);
    }

    public void enviarDatos(MensajeDto msj) {
        // establish a connection 
        try {
            socket = new Socket(ipServidor, puertoDatosServiror);
            System.out.println("Connected");

            // takes input from terminal 
            input = new DataInputStream(System.in);

            // sends output to the socket 
            out = new DataOutputStream(socket.getOutputStream());
        } catch (UnknownHostException u) {
            System.out.println(u);
        } catch (IOException i) {
            System.out.println(i);
        }

        // string to read message from input 
        String jsonMensaje = "";

        try {
            jsonMensaje = json.toJson(msj);
            out.writeUTF(jsonMensaje);
        } catch (IOException i) {
            System.out.println(i);
        }

        // close the connection 
        try {
            input.close();
            out.close();
            socket.close();
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    public void enviarReaccion(MensajeDto msj) {
        // establish a connection 
        try {
            socket = new Socket(ipServidor, puertoReaccionServiror);
            System.out.println("Connected");

            // takes input from terminal 
            input = new DataInputStream(System.in);

            // sends output to the socket 
            out = new DataOutputStream(socket.getOutputStream());
        } catch (UnknownHostException u) {
            System.out.println(u);
        } catch (IOException i) {
            System.out.println(i);
        }

        // string to read message from input 
        String line = "";

        try {
            line = json.toJson(msj);
            out.writeUTF(line);
        } catch (IOException i) {
            System.out.println(i);
        }

        // close the connection 
        try {
            input.close();
            out.close();
            socket.close();
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    public void recibirActualizacionDatos(int port) {
        try {
            server = new ServerSocket(port);
            System.out.println("Cliente started");
            Thread t = new Thread(() -> {
                while (true) {
                    try {
                        System.out.println("Waiting for a server ...");
                        socket = server.accept();
                        System.out.println("Server accepted");

                        // takes input from the client socket
                        in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

                        String jsonDato = "";
                        try {
                            jsonDato = in.readUTF();
                            mensaje = json.fromJson(jsonDato, MensajeDto.class);
                            evaluarActualizacion(mensaje);
                        } catch (IOException i) {
                            System.out.println(i);
                        }

                        // close connection
                        socket.close();
                        in.close();
                    } catch (IOException ex) {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            hilos.add(t);
            t.start();
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    private void evaluarActualizacion(MensajeDto msj) { 
        
        AppContext.getInstance().set("Mensaje", msj);
        
        if (msj.getPeticion() != null) {
            if (msj.getPeticion().equalsIgnoreCase("MostrarTiempoInicioPartida")) {
                mostrarMensaje(msj);
                mostrarTiempoEspera();
            }
        }

       
        if (msj.getPeticion() != null) {
            if (msj.getPeticion().equalsIgnoreCase("PasarPantallaEspera")) {
                pasarPantallaEspera();

            }
        }
          if (msj.getPeticion() != null) {
            if (msj.getPeticion().equalsIgnoreCase("MostrarMensajeServidorClienteFueraTimepo")) {
                 Platform.runLater(() -> {
                    msjPopUp.notifyMensajeImagenPersonalizada("Error registro", msj.getMensaje(),"virus_cliente/resources/icon.png");
                });
            }
        }
          
        
        if (msj.getPeticion() != null) {
            if (msj.getPeticion().equalsIgnoreCase("MostarErrorAgregarNombreExistente")) {
                Platform.runLater(() -> {
                    msjPopUp.notifyMensajeError("Error registro", "Ya que existe otro usuario con su nombre\n"
                            + "O con el codigo de maquina");
                });
            }
        }
       
        if (msj.getPeticion() != null) {
            if (msj.getPeticion().equalsIgnoreCase("InciarPartidaCambioModoJuego")) {
                pasarPantallaModoJuego();
            }
        }

    }
//Area de cambio de pantallas
    private void pasarPantallaEspera() {
        InicioController pantallaInicio = (InicioController) AppContext.getInstance().get("InicioController");
        pantallaInicio.pasarPantallaEspera();
    }
    private void mostrarTiempoEspera(){
       EsperaController esperaController = (EsperaController) AppContext.getInstance().get("EsperaController");
       esperaController.iniciarTiempoInicioPartida();  
    }
    private void pasarPantallaModoJuego() {
        EsperaController esperaController = (EsperaController) AppContext.getInstance().get("EsperaController");
        esperaController.irPrincipal();
    }
 //area de mostrar mensajes al usuario
    private void mostrarMensaje(MensajeDto msj){
       Platform.runLater(() -> {
                msjPopUp.notifyMensajeInformacion("Info",  msj.getMensaje());
            }); 
    }
    private void recibirActualizacionReaccion(int port) {
        try {
            server = new ServerSocket(port);
            System.out.println("Client started");
            Thread t = new Thread(() -> {
                while (true) {
                    try {
                        System.out.println("Waiting for a server and datos ...");
                        socket = server.accept();
                        System.out.println("Server accepted");

                        // takes input from the client socket
                        in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

                        String jsonDato = "";
                        try {
                            jsonDato = in.readUTF();
                            reaccion = json.fromJson(jsonDato, MensajeDto.class);

                        } catch (IOException i) {
                            System.out.println(i);
                        }

                        // close connection
                        socket.close();
                        in.close();
                    } catch (IOException ex) {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            hilos.add(t);
            t.start();
        } catch (IOException i) {
            System.out.println(i);
        }
    }
}
