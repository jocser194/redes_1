/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import virus_cliente.util.FlowController;
/**
 *
 * @author jocse
 */
public class Virus_cliente extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        
      
        primaryStage.setMinWidth(1280);
        primaryStage.setMinHeight(768);
        primaryStage.getIcons().add(new Image("virus_cliente/resources/icon.png"));
        primaryStage.setTitle("Virus");
        FlowController.getInstance().InitializeFlow(primaryStage, null);
        FlowController.getInstance().goViewInWindow("Inicio");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
