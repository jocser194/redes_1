/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.model;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;

/**
 *
 * @author jocse
 */
public class Baraja {
    private List<Cartas> cartas = new ArrayList<Cartas>();

    public Baraja() {
        crearBaraja();
    }
    
    
    private void crearBaraja(){
        for (int i = 0; i < 68; i++) {
            Cartas c = new Cartas(80, 90);
            c.setImage(buscarImagen(i));
            c.setNumeroCarta(i);
            cartas.add(c);
        }
    }
    public Cartas getCarta(Integer i){
        
        for (Cartas carta : cartas) {
            if (carta.getNumeroCarta()==i) {
                return carta;
            }
        }
        
        return null;
    }
    public Image buscarImagen(Integer i){
        //organos
        if (i<5) {
            return new Image("virus_cliente/resources/Organos/corazonR.png");
        }
        if (i<10) {
            return new Image("virus_cliente/resources/Organos/estomagoV.png");
        }
        if (i<15) {
            return new Image("virus_cliente/resources/Organos/cerebro_A1.png");
        }
        if (i<20) {
            return new Image("virus_cliente/resources/Organos/huesosA.png");
        }
        if (i<21) {
            return new Image("virus_cliente/resources/Organos/OrganoM.png");
        }
        //Virus
        if (i<25) {
            return new Image("virus_cliente/resources/Virus/virus_rojo.png");
        }
        if (i<29) {
            return new Image("virus_cliente/resources/Virus/virus_verde.png");
        }
        if (i<33) {
            return new Image("virus_cliente/resources/Virus/virus_azul.png");
        }
        if (i<37) {
            return new Image("virus_cliente/resources/Virus/virus_amarillo.png");
        }
        if (i<38) {
            return new Image("virus_cliente/resources/Virus/virusMulticolor.png");
        }
        //Medicina
        if (i<42) {
            return new Image("virus_cliente/resources/Medicina/medicinaR.png");
        }
        if (i<46) {
            return new Image("virus_cliente/resources/Medicina/medicinaV.png");
        }
        if (i<50) {
            return new Image("virus_cliente/resources/Medicina/medicinaA.png");
        }
        if (i<54) {
            return new Image("virus_cliente/resources/Medicina/medicinaAM.png");
        }
        if (i<58) {
            return new Image("virus_cliente/resources/Medicina/medicinaMult.png");
        }
        //Tratamientos
        if (i<60) {
            return new Image("virus_cliente/resources/Tratamientos/transplante.png");
        }
        if (i<63) {
            return new Image("virus_cliente/resources/Tratamientos/robo.png");
        }
        if (i<66) {
            return new Image("virus_cliente/resources/Tratamientos/contajio.png");
        }
        if (i<67) {
            return new Image("virus_cliente/resources/Tratamientos/guantesL.png");
        }
        if (i<68) {
            return new Image("virus_cliente/resources/Tratamientos/errorMedico.png");
        }
        return null;
    }
    //agregar Dag and Drop

}
