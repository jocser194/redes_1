/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jocse
 */
public class MensajeDto {
    
 //Datos iniciales de la clase
    @SerializedName("peticion")
    private String peticion;
    //Datos iniciales de la clase
    @SerializedName("peticionMostrarMensaje")
    private String peticionMostrarMensaje;
    @SerializedName("tiempoInicioPartida")
    private String tiempoInicioPartida;    
    @SerializedName("metodo")
    private String metodo;
    @SerializedName("mensaje")
    private String mensaje;

    //listas de jugadores activos en la partida
    @SerializedName("jugadoresEnLinea")
    private List<JugadorLinea> jugadoresEnLinea = new ArrayList<JugadorLinea>();
    @SerializedName("msjServidorCliente")
    //mensaje del servidor al jugador
    private String msjServidorCliente;
    
    @SerializedName("cartasDesecho")
    private List<Integer> cartasDesecho = new ArrayList<Integer>();

    public MensajeDto() {
        
    }
    
    public void  cargarDatosJugador() {
       
    }
    
    public String getPeticion() {
        return peticion;
    }

    public void setPeticion(String peticion) {
        this.peticion = peticion;
    }

    public String getMetodo() {
        return metodo;
    }

    public void setMetodo(String metodo) {
        this.metodo = metodo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


    public String getMsjServidorCliente() {
        return msjServidorCliente;
    }

    public void setMsjServidorCliente(String msjServidorCliente) {
        this.msjServidorCliente = msjServidorCliente;
    }

    public List<JugadorLinea> getJugadoresEnLinea() {
        return jugadoresEnLinea;
    }

    public void setJugadoresEnLinea(List<JugadorLinea> jugadoresEnLinea) {
        this.jugadoresEnLinea = jugadoresEnLinea;
    }

    public String getPeticionMostrarMensaje() {
        return peticionMostrarMensaje;
    }

    public void setPeticionMostrarMensaje(String peticionMostrarMensaje) {
        this.peticionMostrarMensaje = peticionMostrarMensaje;
    }

    public String getTiempoInicioPartida() {
        return tiempoInicioPartida;
    }

    public void setTiempoInicioPartida(String tiempoInicioPartida) {
        this.tiempoInicioPartida = tiempoInicioPartida;
    }

    public List<Integer> getCartasDesecho() {
        return cartasDesecho;
    }

    public void setCartasDesecho(List<Integer> cartasDesecho) {
        this.cartasDesecho = cartasDesecho;
    }


 
}
