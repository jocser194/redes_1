/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virus_cliente.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author jocse
 */
public class Cartas extends ImageView {
    private Integer numeroCarta;
    private String color;
    private String tipo;//puede ser Organo, Virus, inmunidad, vacuna,etc...

    public Cartas(Integer w,Integer h) {
        
        redimencionar(w,h);
    }
    
    
    public void redimencionar(Integer w,Integer h){
        this.setFitWidth(w);
        this.setFitHeight(h);
    }

    public Integer getNumeroCarta() {
        return numeroCarta;
    }

    public void setNumeroCarta(Integer numeroCarta) {
        this.numeroCarta = numeroCarta;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
}
